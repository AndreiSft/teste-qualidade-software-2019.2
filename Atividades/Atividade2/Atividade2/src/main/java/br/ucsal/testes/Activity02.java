package br.ucsal.testes;

import java.util.Scanner;

public class Activity02 {

	private Scanner sc = new Scanner(System.in);

	public Integer getNumber() {
		Integer number = getInteger("Informe um n�mero de 1 at� 1000: ");
		return number;
	}

	public void showResult(Integer number) {
		if (isPrime(number))
			System.out.println("O " + number + " � primo.");
		else {
			System.out.println("O " + number + " n�o � primo.");
		}
	}

	public boolean isPrime(Integer number) {
		Integer m = number / 2;

		if (number == 2)
			return true;
		if (number <= 1)
			return false;
		if (number % 2 == 0)
			return false;
		for (int i = 2; i <= m; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		
		return true;
	}

	private Integer getInteger(String message) {
		while (true) {
			try {
				System.out.println(message);
				Integer number = sc.nextInt();
				sc.nextLine();
				validate(number);
				return number;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private void validate(Integer number) throws Exception {
		if (number < 1 || number > 1000) {
			throw new Exception("S� � permitido n�meros entre 1 e 1000");
		}
	}
}
