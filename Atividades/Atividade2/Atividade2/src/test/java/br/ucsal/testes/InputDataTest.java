package br.ucsal.testes;

import java.io.ByteArrayInputStream;

import org.junit.*;

public class InputDataTest {
	
	@Test
	public void InputDataTest() {
		Activity02 atv02 = new Activity02();
		
		String numberInput = "10";
		
		ByteArrayInputStream inputTest = new ByteArrayInputStream(numberInput.getBytes());
		System.setIn(inputTest);
		
		Integer expected = 10;
		Integer actual = atv02.getNumber();
		
		Assert.assertEquals(expected, actual);
	} 
	
}
