package br.ucsal.testes;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class IsPrimeTest {
	
	@Parameters(name="Test number prime {0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{2, true}, {9, false}, {4, true}, {100, false}, {1000, false}, 
			{107, true}, {859, true}, {860, false}, {883, true}, {294, false}
		});
	}
	
	@Parameter(0) 
	public Integer resultCurrent;

	@Parameter(1)
	public Boolean resultAwaited;
	
	@Test
	public void isPrimeTest() {
		Activity02 activity = new Activity02();
		Assert.assertEquals(resultAwaited, activity.isPrime(resultCurrent));
	}
}
